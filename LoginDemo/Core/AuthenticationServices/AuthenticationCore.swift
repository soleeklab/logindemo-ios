//
//  AuthenticationCore.swift
//  Authentication
//
//  Created by sameh on 1/3/18.

import Foundation
import Alamofire


internal extension Authentication
{
    class func makeRequest(parameters:[String:Any],method:HTTPMethod = .post, url:authUrls, completion:@escaping ( _ response:AuthenticationResponse?) -> Void)
    {
        Alamofire.request(
           authUrls.base.rawValue +  url.rawValue,
            method: method,
            parameters: parameters
//            encoding:JSONEncoding.prettyPrinted,//,
//            headers:["x-mock-response-code":UserDefaults.standard.object(forKey: "token") as! String]
            )
            .validate()
            .validate(contentType: ["application/json"])
            .responseJSON
            {
                response in
                debugPrint(response)
                print(String(data: (response.request?.httpBody)!, encoding:String.Encoding.utf8)!)

                switch response.result
                {
                case .success(let result):
                    
                    guard let result = result as? NSDictionary
                    else
                    {
                        return
                    }
                    
                    let authenticationResponse = AuthenticationResponse(result)
                    
                    completion(authenticationResponse)
                    
                    break
                    
                case .failure(_):
                    completion(nil)
                }
        }
    }
    
    class func basicCheck(parameters:[authParameters:Any?]?) -> [String:Any]?
    {
        if !isConnected()
        {
            return nil
        }
        //Getting paramters to be string
        let preparedParamters = preparingData(parameters: parameters)
        return preparedParamters
    }
    
    class func allowNonUser(completion:@escaping (_ success: Bool,_ response: String?) -> Void)
    {
        let emptyUser:[String:Any] = ["user":""]
        UserDefaults.standard.setArchived(emptyUser, forKey: "userData")
        completion(true,nil)
    }
    
    class func emptyStringValidation(parameters:[String:Any])->Bool
    {
        for parameter in parameters
        {
            if (((parameter.value as? String)?.count ?? 1 ) < 1)
            {
                return false
            }
        }
        return true
    }
    
    class func preparingData(parameters:[authParameters:Any?]?)->[String:Any]
    {
        var iParameters = [String:Any]()
        
        guard let parameters = parameters
            else
        {
            return iParameters
        }
        
        for parameter in parameters
        {
            //Nil check
            guard parameter.value != nil else { continue }
            iParameters[parameter.key.rawValue] = parameter.value
        }
        
        //iParameters[authParameters.language.rawValue] = LanguageManager.getCurrentLanguage()
        //iParameters[authParameters.deviceType.rawValue] = "2"
        
        return iParameters
    }
    
}
