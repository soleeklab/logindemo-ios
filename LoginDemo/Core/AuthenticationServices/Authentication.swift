import Foundation
import Alamofire


class Authentication
{
    //Main Actions
    class func login(user:User,completion:@escaping (_ response : ApiResponse
        ) -> Void)
    {
        loginLogic(user: user, type: .login){ response in completion(response)}
    }
    
    class func register(user:User , completion:@escaping (_ response : ApiResponse ) -> Void)
    {
        registerLogic(user: user, type: .register, completion: completion)
    }
    
    private class func loginLogic(user:User,type:authParameters,completion:@escaping (_ response : ApiResponse) -> Void)
    {
        let parameters = user.convertToDictionary() as! [String:Any]
//        let url = (type == .login ) ? authUrls.login : authUrls.socialLogin
        
        makeRequest(parameters: parameters, url: .login)
        {
            response in
            
            guard let response = response
                else
            {
                completion(ApiResponse(success: false, message: "unableLogin"))
                return
            }
            
            completion(response.api)
        }
    }
    
    class func registerLogic(user:User ,type:authParameters, completion:@escaping (_ response : ApiResponse) -> Void)
    {
        let parameters = user.convertToDictionary() as! [String:Any]
//        let url = (type == .register ) ? authUrls.register : authUrls.socialRegister
        
        makeRequest(parameters: parameters, url: .register)
        {
            response in
            
            guard let response = response
                else
            {
                completion(ApiResponse(success: false, message: "unableLogin".localized))
                return
            }
            
            completion(response.api)
        }
    }
    
    class func verifyUser(code:String,user:User,completion:@escaping (_ response : ApiResponse) -> Void)
    {
        let parameters = NSMutableDictionary(dictionary: user.convertToDictionary()!)
        parameters.addEntries(from: ["verification_code":code])
        
        makeRequest(parameters: parameters as! [String:Any] , url: .verifyUser)
        {
            response in
            
            guard let response = response
                else
            {
                completion(ApiResponse(success: false, message: "unableLogin".localized))
                return
            }
            
            let registerResponse = self.saveUserData(response)
            
            guard registerResponse.success
                else
            {
                completion(registerResponse)
                return
            }
            
            completion(registerResponse)
        }
    }

    class func logout(completion:@escaping (_ response: ApiResponse) -> Void)
    {
        
        /*makeRequest(parameters: serviceCode, user: User(), url: authUrls.logout)
        {
            response in
            
            guard let response = response
                else
            {
                completion(LoginResponse(success: false, message: "unableLogin".localized))
                return
            }
            
            UserDefaults.standard.removeObject(forKey: "UserData")
            let loginResponse = response.status
            completion(loginResponse)
        }*/
    }
}
