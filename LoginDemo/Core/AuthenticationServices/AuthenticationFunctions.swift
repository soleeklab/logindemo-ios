//
//  AuthenticationFunctions.swift
//  Authentication
//
//  Created by sameh on 1/3/18.

import Foundation

extension Authentication
{
    class func saveUserData(_ response:AuthenticationResponse)->ApiResponse
    {
    
            guard response.api.success,
                let user = response.user // no success flag where sent
        else
        {
            return response.api
        }
        
        UserDefaults.standard.set(try? PropertyListEncoder().encode(user), forKey:"UserData")

        User.distroySharedUser()
        
        return response.api
    }
    
    class func saveUser(user:User)->ApiResponse
    {
        do
        {
            UserDefaults.standard.set(try PropertyListEncoder().encode(user), forKey:"UserData")
            User.distroySharedUser()
            return ApiResponse(success: false, message: "")
        }
        catch
        {
            return ApiResponse(success: false, message: "")
        }
    }
    
    class func saveToken(token:String,completion:@escaping (_ response:ApiResponse) -> Void)
    {
        //let parameters = [authParameters.token:token]
        //Authentication.checkLogin(extraParameters:parameters, completion: completion)
    }
    /*
    class func checkLogin(extraParameters:[authParameters:Any]?,completion:@escaping (_ response:LoginResponse) -> Void)
    {
        //Check for login method Object - to know if user has logged in before or not
        guard (UserDefaults.standard.archivedObject(forKey: "UserData") as? User) != nil
        else
        {
            completion(LoginResponse(success: false, message: nil))
            return
        }
        
        guard let userCredintials = UserDefaults.standard.archivedObject(forKey: "UserCredentials") as? User
        else
        {
            completion(LoginResponse(success: false, message: nil))
            return
        }
        
        login(user: userCredintials)
        {
            response  in
            completion(response)
        }
    }*/
    
}

