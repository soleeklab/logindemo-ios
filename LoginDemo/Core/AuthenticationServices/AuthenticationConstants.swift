import Foundation

extension Authentication
{
    enum authParameters:String
    {
        case register = "Register"
        case login = "Login"
        case verifyUser = "VerifyUser"
        case logout = "Logout"
    }
    
    enum authUrls:String
    {
        case base = "http://52.174.22.188/yamam/public/api/" 
        case login = "auth"
        case register = "auth/signup"
        case verifyUser =  "auth/verify"
        case logout = "http://dots.westus2.cloudapp.azure.com:9090/SDN/client/logout"
        
    }
    
}
