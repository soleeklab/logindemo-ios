//
//  APIInterface.swift
//  Gafsha
//
//  Created by sameh on 12/18/17.
//  Copyright © 2017 Revival. All rights reserved.
//

import Foundation
import Alamofire


class APIService
{
    static private let configuration = URLSessionConfiguration.default
    static var afManager = Alamofire.SessionManager(configuration: configuration)
    private let baseUrl =  "http://52.174.22.188/yamam/public/api/"
  
    enum URLs:String
    {
        case savedPlaces = "user/places"
        case googleDirectionService = "https://maps.googleapis.com/maps/api/directions/json"
        case resendCode = "auth/resend"
    }
    
    enum requestType
    {
        case api
        case external
    }
    
    @discardableResult init(parameters:[String:Any]?,URL:URLs,method:HTTPMethod = .post,requestType:requestType = .api, completion: @escaping (_ response:ApiResponse) -> ())
    {
        makeRequest(parameters: parameters, URL: URL, requestType:requestType, method: method, completion: completion)
    }
    
    static func cancelAllRequests()
    {
        afManager.session.invalidateAndCancel()
        afManager = Alamofire.SessionManager(configuration: configuration)
    }
    
    private func makeRequest(parameters:[String:Any]?,URL:URLs,requestType:requestType,method:HTTPMethod , completion: @escaping (_ response:ApiResponse) -> ())
    {
        let url = (requestType == .api) ? baseUrl + URL.rawValue : URL.rawValue
        
        APIService.afManager.request(
            url,
            method: method,
            parameters: parameters,
            headers:["Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vNTIuMTc0LjIyLjE4OC95YW1hbS9wdWJsaWMvYXBpL2F1dGgvdmVyaWZ5IiwiaWF0IjoxNTI4MDY0MjA0LCJleHAiOjM3NTI4MDY0MjA0LCJuYmYiOjE1MjgwNjQyMDQsImp0aSI6IjJpYmJNb1dpNzdsRUhUam8iLCJzdWIiOjEsInBydiI6IjI1OTA4ZTEwNDNiM2VhZTNiZDVlNTE3OWUzODA1YTE5MGNmN2Y4YTUifQ.gz1kyoIuQZMhBtVkgz41t3nZbv9DwzGsWejKha9MreU"]
            )
            
            .validate()
            .validate(contentType: ["application/json"])
            
            .responseJSON
            {
                response in
                debugPrint(response)
                
                guard /*let validatedResponse = self.validateResponseForAPI(response: response) else { return }*/
                let validatedResponse = (requestType == .api) ?  self.validateResponseForAPI(response: response) : self.validateExternalResponse(response: response)
                else { return }
                completion(validatedResponse)
                
        }
    }
    
    private func validateResponseForAPI(response:DataResponse<Any>)->(ApiResponse?)
    {
        switch response.result
        {
        case .success(let result):
            
            guard let JsonArray = result as? NSDictionary else
            {
                return (nil)
            }
            
            return(ApiResponse(JsonArray))
            
            
        case .failure( _):
            return (nil)
        }
    }
    
    private func validateExternalResponse(response:DataResponse<Any>)->(ApiResponse?)
    {
        switch response.result
        {
        case .success(let result):
            
            guard let JsonArray = result as? NSDictionary else
            {
                return ApiResponse(success: false, message: "")
            }
            
            return ApiResponse(code: 0, success: true, message: "", error: nil, data: JsonArray)

            
            
        case .failure( _):
            return ApiResponse(success: false, message: "")
        }
    }
    
    
}
