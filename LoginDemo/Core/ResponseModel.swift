import Foundation

class AuthenticationResponse
{
    
    var api:ApiResponse
    private var userObject:Any?
    lazy var user:User? =
    {
        do
        {
            let jsonData = try JSONSerialization.data(withJSONObject: userObject!, options: .prettyPrinted)
            return try JSONDecoder().decode(User.self, from: jsonData)
        }
        catch
        {
            print("error parsing")
            print(error.localizedDescription)
        }
        return nil
    }()
    
    
    init( _ data:NSDictionary)
    {
        api = ApiResponse(data)
        userObject = api.data
    }
}
