//
//  FlipOverTransitionP.swift
//  Yamam
//
//  Created by sameh on 6/9/18.
//  Copyright © 2018 soleek. All rights reserved.
//

import Foundation

protocol FlipOverTransitionP:UIViewControllerTransitioningDelegate
{
    var transition:KWTransition! {get set }
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning?
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning?
}

extension FlipOverTransitionP where Self:UIViewController
{
    func dismissed()->UIViewControllerAnimatedTransitioning
    {
        self.transition.action = .present
        return self.transition
    }
    
    func configureViewControllerAnimation()
    {
        transition = KWTransition.manager()
        transition.style = .fadeBackOver
    }
    
    func presented()->UIViewControllerAnimatedTransitioning
    {
        self.transition.action = .dismiss
        return self.transition
    }
}



/*fileprivate extension UIViewController:UIViewControllerTransitioningDelegate
{
      func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning?
    {
        
//        self.transition.action = .present
//        return self.transition
    }
    
    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning?
    {
        self.transition.action = .dismiss
        return self.transition
    }
}
*/
