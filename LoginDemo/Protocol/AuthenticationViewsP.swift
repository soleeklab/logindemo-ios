//
//  AuthenticationViewsLogic.swift
//  Yamam
//
//  Created by sameh on 5/21/18.
//  Copyright © 2018 soleek. All rights reserved.
//

import Foundation

protocol AuthenticationViewModel:class
{
    var user:User! { get set }
    var selectedCountryCode:Country { get set }
    var botQuestions:[ChatMessage] { get set }
    func validate(question:ChatMessage?,answer:String)->String?
    var delegate:ConversationDelegate? { get set}
}
