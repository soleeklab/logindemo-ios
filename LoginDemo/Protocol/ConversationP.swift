//
//  ConversationProtocol.swift
//  Yamam
//
//  Created by sameh on 5/19/18.
//  Copyright © 2018 soleek. All rights reserved.
//

import Foundation
import UIKit

protocol ChatMessageDelegate:class
{
    func userMessageEdited(index:Int,newMessage:String?)
    func userStartedEdit(index:Int)
}

protocol ConversationDelegate:class
{
    func verifyUser(user:User,code:String)
    func registerUser(user:User)
    func loginUser(user:User)
    func phoneNumberUI()
    func selectedCountryCode(country:Country)
    var botQuestions:[ChatMessage] { get set }
}
