//
//  GradientP.swift
//  Yamam
//
//  Created by sameh on 5/22/18.
//  Copyright © 2018 soleek. All rights reserved.
//

import Foundation

protocol GradientProtocol:class
{
    var gradient:CAGradientLayer { get set }
    var horizontalGradient:Bool  {get set }
    var gradientColorOne:UIColor {get set}
    var gradientColorTwo:UIColor {get set}
    var gradientBorder:Bool {get set}
    var gradientBackground:Bool {get set}
    func gradientLayout()
}

extension GradientProtocol where Self:UIView
{
    func gradientLayout()
    {
        if gradientBorder
        {
            gradient.removeFromSuperlayer()
            gradientBorder = true
        }
        if gradientBackground
        {
            gradient.removeFromSuperlayer()
            gradientBackground = true
        }
    }
}
