//
//  ProgressbarUI.swift
//  Yamam
//
//  Created by sameh on 5/19/18.
//  Copyright © 2018 soleek. All rights reserved.
//

import Foundation
import UIKit

protocol ProgressBarUIDelegate:class
{
    func progressDidFinish()
}

class ProgressBarUI:UIView
{
    @IBOutlet weak var progressBar:UIProgressView!
    @IBOutlet weak var countDown:UILabel!
    private var currentValue:Float = 0
    var endValue:Int = 100
    var timer = Timer()
    var delegate:ProgressBarUIDelegate?
    
    @IBInspectable var totalValue:Float = 100
    {
        didSet
        {
            countDown.text = "0:\(totalValue)"
            self.currentValue = self.totalValue
        }
    }

    @IBInspectable var startValue:Float = 0
    {
        didSet
        {
            self.progressBar.progress = startValue
        }
    }
    
    @IBInspectable var timeInterval:Double = 0
    {
        didSet
        {
            setTimer(interval: timeInterval)
        }
    }
    

    override init(frame: CGRect)
    {
        super.init(frame: frame)
        common()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        common()
    }
    
    func common()
    {
        let ui = loadNib(name: "Progressbar")
        ui.frame = bounds
        addSubview(ui)
        progressBar.progress = startValue
    }
    
    func setTimer(interval:Double)
    {
        timer.invalidate()
        progressBar.progress = startValue
        currentValue = totalValue
        timer = Timer.scheduledTimer(withTimeInterval: interval , repeats: true)
        {
            _ in
            if self.currentValue < 1
            {
                self.progressBar.progress = 1
                self.timer.invalidate()
                self.delegate?.progressDidFinish()
            }
            
            self.progressBar.progress += Float(interval) / self.totalValue
            self.currentValue -=  Float(interval)
            let value = Int(self.currentValue)
            self.countDown.text = "0:\(value < 10 ? "0":"")\(value)"
        }
    }
    
}
