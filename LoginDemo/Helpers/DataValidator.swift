//
//  DataValidator.swift
//  Yamam
//
//  Created by sameh on 5/20/18.
//  Copyright © 2018 soleek. All rights reserved.
//

import Foundation

class DataValidator
{
    class func validateEmail(_ email:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    class func validatePhone(value: String) -> Bool
    {
        let PHONE_REGEX = "^[0-9]{6,11}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
}
