//
//  ReachiblityExtension.swift
//  ZohurElReyad
//
//  Created by sameh on 8/25/17.
//  Copyright © 2017 Atiaf. All rights reserved.
//

import Foundation
import UIKit


func isConnected()->Bool {
    guard let status = Network.reachability?.isReachable else { return false }
    return status
}

extension AppDelegate{
    
    func startReachibility(){
        do {
            Network.reachability = try Reachability(hostname: "www.google.com")
            do {
                try Network.reachability?.start()
            } catch let error as Network.Error {
                print(error)
            } catch {
                print(error)
            }
        } catch {
            print(error)
        }
    }
    
    @objc func statusManager(_ notification: NSNotification) {
        UIForNetwork()
    }
    
    fileprivate func UIForNetwork(){
        if (!isConnected()){
            print("offlie view")
            //let storyboard = UIStoryboard(name: "Others", bundle: nil)
            //let offlineView  = storyboard.instantiateViewController(withIdentifier: "OfflineView") as! OfflineView
            //window?.rootViewController?.present(offlineView, animated: false, completion: nil)
        }
    }
    
}
