import Foundation
import UIKit

extension UIViewController
{
    func removeBackButtonText()
    {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        self.tabBarController?.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style: .plain, target: self, action: nil)
    }
}

extension AppDelegate
{
    func customNav(){
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        // Sets shadow (line below the bar) to a blank image
        UINavigationBar.appearance().shadowImage = UIImage()
        // Sets the translucent background color
        UINavigationBar.appearance().backgroundColor = .clear
        // Set translucent. (Default value is already true, so this can be removed if desired.)
        UINavigationBar.appearance().isTranslucent = true
        
        UINavigationBar.appearance().backIndicatorImage = #imageLiteral(resourceName: "Back Arrow")
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "Back Arrow")
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor(hex:0x003A70)]

        //UIBarButtonItem.appearance().setBackButtonBackgroundImage(#imageLiteral(resourceName: "Back Arrow"), for: .normal, barMetrics: .default)
    }
}
extension UINavigationController
{
    func transparentNav()
    {
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.shadowImage = UIImage()
    }
    
    func previousVC()->UIViewController?
    {
        let vcs = viewControllers
        if vcs.count > 1
        {
            return vcs[vcs.count - 2]
        }
        else
        {
            return nil
        }
        
        
    }
    

}
