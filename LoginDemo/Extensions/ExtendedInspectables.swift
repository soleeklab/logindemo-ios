import UIKit
import Foundation


extension UIView
{
    @IBInspectable var borderColor:UIColor{
        get{
            return UIColor(cgColor: layer.shadowColor!)
        }
        
        set{
            layer.shadowColor = newValue.cgColor
            layer.borderColor = newValue.cgColor
        }
    }
    
    @IBInspectable var bottomBorderWeight: Double {
        get
        {
            return Double(layer.shadowOffset.height)
        }
        
        set
        {
            if self is UIButton
            {
                let lineView = UIView(frame: CGRect(x: 0, y: frame.height - 5, width: frame.width, height: 1))
                lineView.backgroundColor = borderColor
                self.addSubview(lineView)
            }
            else
            {
                
            
            layer.masksToBounds = false
            layer.shadowOffset = CGSize(width: 0.0, height: newValue)
            layer.shadowOpacity = 1.0
            layer.shadowRadius = 0.0
            }
            //To work with text , change its background to white
            //layer.backgroundColor = UIColor.white.cgColor
        }
    }
    
    @IBInspectable var shadow:CGFloat{
        get{
            return layer.shadowRadius
        }
        
        set{
            layer.shadowOffset = CGSize(width: 3.0, height: 2.0)
            // set the radius
            layer.shadowRadius = newValue
            // change the color of the shadow (has to be CGColor)
            layer.shadowColor = UIColor.gray.cgColor
            // display the shadow
            layer.shadowOpacity = 1.0
        }
    }

    @IBInspectable var cornerRadius:CGFloat{
        get{
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderWidth:CGFloat{
        get{
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
}
