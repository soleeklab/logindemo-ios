//
//  ExtendedView.swift
//  Yamam
//
//  Created by sameh on 5/22/18.
//  Copyright © 2018 soleek. All rights reserved.
//

import Foundation
import UIKit

class ExtendedView:UIView,GradientProtocol
{
    var gradient = CAGradientLayer()

    @IBInspectable var horizontalGradient:Bool  = false
    @IBInspectable var gradientColorOne:UIColor = .clear
    @IBInspectable var gradientColorTwo:UIColor = .clear
    @IBInspectable var gradientBorder:Bool = false
        {
        didSet
        {
            if gradientBorder
            {
                gradient.colors = [gradientColorOne.cgColor, gradientColorTwo.cgColor]
                makeGradientBorder(gradient)
            }
        }
    }
    @IBInspectable var makeCircle:Bool = false {
        didSet
        {
            if makeCircle
            {
                layer.masksToBounds = false
                layer.cornerRadius = frame.height / 2
                clipsToBounds = true
            }
        }
    }
    
    @IBInspectable var gradientBackground:Bool = false
        {
        didSet
        {
            if gradientBackground
            {
                gradient.colors = [gradientColorOne,gradientColorTwo].map { $0.cgColor }
                makeGradientBackground(gradient, horizontal: horizontalGradient)
            }
        }
    }
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        gradientLayout()
        makeCircle = !(!makeCircle)
    }
}
