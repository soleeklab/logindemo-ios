//
//  GradientExtensions.swift
//  Yamam
//
//  Created by sameh on 5/22/18.
//  Copyright © 2018 soleek. All rights reserved.
//

import Foundation
import UIKit

extension UIView
{
    func makeGradientBackground(_ gradColors:CAGradientLayer,horizontal:Bool)
    {
        let grad:CAGradientLayer =
            {
                let gradient = CAGradientLayer()
                gradient.frame = self.bounds
                gradient.colors = gradColors.colors//[gradientColorOne,gradientColorTwo].map { $0.cgColor }
                let orientation = (horizontal) ? (CGPoint(x: 0.0,y: 0.5), CGPoint(x: 1.0,y: 0.5)):(CGPoint(x: 0.0,y: 0.0), CGPoint(x: 0.0,y: 1.0))
                gradient.startPoint = orientation.0
                gradient.endPoint = orientation.1
                return gradient
        }()
        
        self.layer.insertSublayer(grad, at: 0)
    }
    
    func makeGradientBorder(_ gradColors:CAGradientLayer)
    {
        let gradient:CAGradientLayer =
            {
                let gradient = CAGradientLayer()
                gradient.frame =  CGRect(origin: CGPoint.zero, size: self.frame.size)
                gradient.colors = gradColors.colors
                return gradient
        }()
        
        let shape:CAShapeLayer =
        {
            let shape = CAShapeLayer()
            shape.lineWidth = 4
            shape.path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
            shape.strokeColor = UIColor.black.cgColor
            shape.fillColor = UIColor.clear.cgColor
            return shape
        }()
        
        gradient.mask = shape
        self.layer.addSublayer(gradient)
    }
}
