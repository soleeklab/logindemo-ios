//
//  ExtendedTextField.swift
//  Yamam
//
//  Created by sameh on 5/19/18.
//  Copyright © 2018 soleek. All rights reserved.
//

import Foundation
import UIKit

class ExtendedTextField : UITextField {
    
    var padding = UIEdgeInsetsMake(0, 0, 0, 0)

    @IBInspectable var left:CGFloat = 0
        {
        didSet{
            padding.left = left
        }
    }
    @IBInspectable var right:CGFloat = 0
        {
        didSet{
            padding.right = right
        }
    }
    @IBInspectable var top:CGFloat = 0
        {
        didSet{
            padding.top = top
        }
    }
    @IBInspectable var bottom:CGFloat = 0
        {
        didSet{
            padding.bottom = bottom
        }
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds,
                                     padding)
    }
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds,
                                     padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds,
                                     padding)
    }
}
