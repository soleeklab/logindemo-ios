import UIKit
import Foundation


class ExtendedButton: UIButton,GradientProtocol {
    
    /* Gradient protocol */
    var gradient = CAGradientLayer()
    
    @IBInspectable var horizontalGradient:Bool  = false
    @IBInspectable var gradientColorOne:UIColor = .clear
    @IBInspectable var gradientColorTwo:UIColor = .clear
    @IBInspectable var gradientBorder:Bool = false
        {
        didSet
        {
            if gradientBorder
            {
                gradient.colors = [gradientColorOne.cgColor, gradientColorTwo.cgColor]
                makeGradientBorder(gradient)
            }
        }
    }
    
    @IBInspectable var gradientBackground:Bool = false
        {
        didSet
        {
            if gradientBackground
            {
                gradient.colors = [gradientColorOne,gradientColorTwo].map { $0.cgColor }
                makeGradientBackground(gradient, horizontal: horizontalGradient)
            }
        }
    }
    /* */
    
    @IBInspectable var ButtonWithIcon:Bool = false
    @IBInspectable var imageToLeft:Bool = true
    @IBInspectable var makeCircle:Bool = false
    {
        didSet
        {
            if makeCircle
            {
                layer.masksToBounds = false
                layer.cornerRadius = frame.height / 2
                clipsToBounds = true
            }
        }
    }
    
    override init (frame : CGRect)
    {
        super.init(frame : frame)
        
    }
    
    required public init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews()
    {
        super.layoutSubviews()
        
        gradientLayout()
        
        if ButtonWithIcon
        {
            if !imageToLeft
            {
                imageEdgeInsets = UIEdgeInsets(top: 0, left: (bounds.width - 25), bottom: 0, right: 5)
                if contentHorizontalAlignment == .left
                {
                    titleEdgeInsets = UIEdgeInsets(top: 0, left: -(imageView?.frame.width ?? 0), bottom: 0, right: 0)
                }
            }
            else
            {
                imageEdgeInsets = UIEdgeInsets(top: 0, left: -5, bottom: 0, right: (bounds.width - 25))
                if contentHorizontalAlignment == .right
                {
                    titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -(imageView?.frame.width ?? 0 ))
                }
            }
        }
        
    }
    
    
    private func left(){
        contentHorizontalAlignment = .left
        let availableSpace = UIEdgeInsetsInsetRect(bounds, contentEdgeInsets)
        let availableWidth = availableSpace.width - imageEdgeInsets.right - (imageView?.frame.width ?? 0) - (titleLabel?.frame.width ?? 0)
        titleEdgeInsets = UIEdgeInsets(top: 0, left: availableWidth / 2, bottom: 0, right: 0)
    }
    
    private func right(){
        semanticContentAttribute = .forceRightToLeft
        contentHorizontalAlignment = .right
        let availableSpace = UIEdgeInsetsInsetRect(bounds, contentEdgeInsets)
        let availableWidth = availableSpace.width - imageEdgeInsets.left - (imageView?.frame.width ?? 0) - (titleLabel?.frame.width ?? 0)
        titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: availableWidth / 2 )
    }
}
