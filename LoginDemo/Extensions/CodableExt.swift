//
//  CodableExtension.swift
//  Yamam
//
//  Created by sameh on 6/6/18.
//  Copyright © 2018 soleek. All rights reserved.
//

import Foundation

extension Sequence
{
    func getData()->Data?
    {
        return try? JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
    }
    
    func getObject<T:Codable>()->T?
    {
        guard
            let data = self.getData() else{ return nil }
        return data.getObject()
    }
}

extension UserDefaults
{
    func saveObject<T:Codable>(rawData:T,forKey key:String)
    {
        guard let encoded = try? JSONEncoder().encode(rawData) else { return }
        
        UserDefaults.standard.set(encoded, forKey: key)
    }
    
    func getObject<T:Codable>(key:String)->T?
    {
        guard let data = UserDefaults.standard.object(forKey: key) as? Data else { return nil }
        return data.getObject()
    }
}
 

extension Data
{
    func getObject<T:Codable>()->T?
    {
        do
        {
            let parsedData  = try JSONDecoder().decode(T.self, from: self)
            return parsedData
        }
        catch let error
        {
            print(error)
        }
        return nil
    }
}
