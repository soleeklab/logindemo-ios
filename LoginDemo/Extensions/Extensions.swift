//
//  extensions.swift
//  Likes
//
//  Created by sameh on 6/9/17.
//  Copyright © 2017 Atiaf. All rights reserved.
//

import Foundation
import UIKit



extension UIView {
    func drawSpinner()->UIActivityIndicatorView
    {
        //Drawing the Spinner
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        spinner.center = self.center
        spinner.color = UIColor.darkGray
        self.addSubview(spinner)
        spinner.startAnimating()
        
        return spinner
    }
    
    func removeSpinner(spinner:UIActivityIndicatorView?)
    {
        if let spinner = spinner
        {
            spinner.stopAnimating()
            spinner.isHidden = true
        }
    }
    
    func loading(withSpinner:Bool)->UIActivityIndicatorView?
    {
        isUserInteractionEnabled = false
        return withSpinner ? drawSpinner() : nil
    }
    
    func loaded(spinner:UIActivityIndicatorView?)
    {
        isUserInteractionEnabled = true
        removeSpinner(spinner: spinner ?? UIActivityIndicatorView())
    }
    
    func setBottomBorder(color: UIColor = UIColor.gray , width: CGFloat)
    {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: width)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
    
    func loadNib(name:String? = nil) -> UIView
    {
        let bundle = Bundle(for: type(of: self))
        let nibName = type(of: self).description().components(separatedBy: ".").last!
        let nib = UINib(nibName: name ?? nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
    }
    
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true)
    {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    func image() -> UIImage {
        let renderer = UIGraphicsImageRenderer(bounds: bounds)
        return renderer.image { rendererContext in
            layer.render(in: rendererContext.cgContext)
        }
    }
    
}

extension AppDelegate{
    func alert(title:String,message:String){
        let topWindow = UIWindow(frame: UIScreen.main.bounds)
        topWindow.rootViewController = UIViewController()
        topWindow.windowLevel = UIWindowLevelAlert + 1
        let alert = UIAlertController(title: title , message: message , preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "confirm"), style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
            topWindow.isHidden = true
        }))
        topWindow.makeKeyAndVisible()
        topWindow.rootViewController?.present(alert, animated: true, completion: nil)
    }
}

extension UIViewController{
    func alert(message: String, title: String = "",action:UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: nil) )
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = action
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func missingFields(spinner:UIActivityIndicatorView,message:String = "MissingFields".localized)
    {
        view.isUserInteractionEnabled = true
        self.view.removeSpinner(spinner: spinner)
        self.alert(message: message)
    }
}


extension UIToolbar {
    
    func ToolbarPiker(saveAction : Selector, cancelAction: Selector) -> UIToolbar {
        
        let toolBar = UIToolbar()
        
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let saveButton = UIBarButtonItem(title: "Save", style: UIBarButtonItemStyle.plain, target: self, action: saveAction)
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: cancelAction)
        
        toolBar.setItems([cancelButton, spaceButton, saveButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        return toolBar
    }
}

extension String {
    var localized: String {
        if let _ = UserDefaults.standard.string(forKey: "i18n_language") {} else {
            // we set a default, just in case
            UserDefaults.standard.set("en", forKey: "i18n_language")
            UserDefaults.standard.synchronize()
        }
        
        let lang = UserDefaults.standard.string(forKey: "i18n_language")
        
        guard let path = Bundle.main.path(forResource: lang, ofType: "lproj") else
        {
            return ""
        }
        let bundle = Bundle(path: path)
        
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
    func encodeUrl() -> String
    {
        return self.addingPercentEncoding( withAllowedCharacters: NSCharacterSet.urlQueryAllowed) ?? ""
    }
    func decodeUrl() -> String
    {
        return self.removingPercentEncoding ?? ""
    }
    
    func toURL () -> URL
    {
        if let url = URL(string: self),
            UIApplication.shared.canOpenURL(url)
        {
            return url
        }
        return URL(string:"http://")!
    }
    
    //Substring
    subscript(value: PartialRangeUpTo<Int>) -> Substring {
        get {
            return self[..<index(startIndex, offsetBy: value.upperBound)]
        }
    }
    
    subscript(value: PartialRangeThrough<Int>) -> Substring {
        get {
            return self[...index(startIndex, offsetBy: value.upperBound)]
        }
    }
    
    subscript(value: PartialRangeFrom<Int>) -> Substring {
        get {
            return self[index(startIndex, offsetBy: value.lowerBound)...]
        }
    }
}

extension Double {
    func rounded( _ places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}



extension Base64{
    func base64ToImage()->UIImage?
    {
        let imageData = self
        let dataDecode = NSData(base64Encoded: imageData, options:.ignoreUnknownCharacters)!
        let avatarImage = UIImage(data: dataDecode as Data)
        return avatarImage
    }
}

extension UserDefaults{
    func setArchived(_ value:Any,forKey:String){
        let data = NSKeyedArchiver.archivedData(withRootObject: value)
        UserDefaults.standard.set(data, forKey: forKey)
    }
    
    func archivedObject(forKey:String)->Any?{
        guard let data = UserDefaults.standard.object(forKey: forKey) as! Data? else { return nil}
        return NSKeyedUnarchiver.unarchiveObject(with: data)
    }
    
}

extension UIApplication {
    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}






extension UIImageView
{
    func setQR(_ string:String)
    {
        let data = string.data(using: .isoLatin1, allowLossyConversion: false)
        
        let filter = CIFilter(name: "CIQRCodeGenerator")!
        
        filter.setValue(data, forKey: "inputMessage")
        filter.setValue("Q", forKey: "inputCorrectionLevel")
        
        let qrcodeImage = filter.outputImage
        
        let scaleX = self.frame.size.width / (qrcodeImage?.extent.size.width)!
        let scaleY = self.frame.size.height / (qrcodeImage?.extent.size.height)!
        
        let transformedImage = qrcodeImage?.transformed(by: CGAffineTransform(scaleX: scaleX, y: scaleY))
        self.image = UIImage(ciImage: transformedImage!)
    }
}

/*
 extension NSDictionary{
 func userDefaultsEncoding() -> Data{
 return NSKeyedArchiver.archivedData(withRootObject: self)
 }
 }
 extension Data{
 func userDefaultsDecoding() ->NSDictionary?{
 if let result = NSKeyedUnarchiver.unarchiveObject(with: self) as! NSDictionary? {
 return result
 }
 return nil
 }
 }*/

extension UICollectionView
{
    func animateReload()
    {
        self.performBatchUpdates(
            {
                let range = Range(uncheckedBounds: (0, self.numberOfSections))
                let indexSet = IndexSet(integersIn: range)
                self.reloadSections(indexSet)
        }, completion: nil)
    }
}

extension UIView
{
    var isDisabled:Bool
    {
        get{
            return !isUserInteractionEnabled
        }
        set{
            alpha = (newValue) ? 0.1 : 1
            isUserInteractionEnabled = !newValue
        }
    }
}

extension UIColor
{
    convenience init(hex: String)
    {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            self.init(cgColor: UIColor.gray.cgColor)
            return
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        self.init(red:  CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0, green:  CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0, blue: CGFloat(rgbValue & 0x0000FF) / 255.0, alpha:  CGFloat(1.0))
        
    }
    
    convenience init(hex: UInt32) {
        self.init(hex: hex, a: 1.0)
    }
    
    convenience init(hex: UInt32, a: CGFloat) {
        self.init(r: (hex >> 16) & 0xff, g: (hex >> 8) & 0xff, b: hex & 0xff, a: a)
    }
    
    convenience init(r: UInt32, g: UInt32, b: UInt32, a: CGFloat) {
        self.init(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: a)
    }
    
    convenience init(red: UInt32, green: UInt32, blue: UInt32) {
        self.init(red: CGFloat(red) / 255, green: CGFloat(green) / 255, blue: CGFloat(blue) / 255, alpha: 1)
    }
    
    static var appColor: UIColor {
        //return UIColor(red:107, green:121, blue:174, alpha:1.0)
        return UIColor(hex: 0x0D4774)
    }
    static var groupGray: UIColor{
        return UIColor(hex: 0xEBEBF1)
    }
}
