//
//  JsonableExt.swift
//  dots
//
//  Created by sameh on 3/7/18.
//  Copyright © 2018 soleek. All rights reserved.
//

import Foundation

protocol Jsonable
{
}

extension Jsonable where Self:Codable
{
    func convertToDictionary()->NSDictionary?
    {
        guard let userData = try? JSONEncoder().encode(self) else { return nil }
        let dictionary = try? JSONSerialization.jsonObject(with: userData, options: .allowFragments) as! NSDictionary
        return dictionary
        
    }
}
