//
//  ChatCell.swift
//  Yamam
//
//  Created by sameh on 5/18/18.
//  Copyright © 2018 soleek. All rights reserved.
//

import Foundation
import UIKit

class ChatCell:UITableViewCell
{
    var message:String!
    @IBOutlet weak var topConstraint:NSLayoutConstraint!
    var animate:Bool  = false
    var cellIndex:Int = 0
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        cellIndex  = 0
        animate = false
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
