//
//  HumanChatCell.swift
//  Yamam
//
//  Created by sameh on 5/18/18.
//  Copyright © 2018 soleek. All rights reserved.
//

import Foundation
import UIKit

class HumanChatCell:ChatCell
{
    override var message:String!{
        didSet{
         messageField.text = message
        }
    }
    
    var index:Int!
    weak var delegate:ChatMessageDelegate?
    
    @IBOutlet weak var messageField:UITextField!
    @IBOutlet weak var bubbleImageView:UIImageView!
    @IBOutlet weak var editButton: UIButton!
    
    
    override func awakeFromNib()
    {
        bubbleImageView.image = #imageLiteral(resourceName: "bubble_sent")
            // #imageLiteral(resourceName: "chat_bubble_sent")
            .resizableImage(withCapInsets:
                UIEdgeInsetsMake(17, 21, 17, 21),
                            resizingMode: .stretch)
            .withRenderingMode(.alwaysTemplate)
        bubbleImageView.tintColor = .white
    }
    
    @IBAction func onEdit( _ sender:UIButton)
    {
        messageField.isUserInteractionEnabled = true
        messageField.becomeFirstResponder()
        delegate?.userStartedEdit(index: index)
    }
  
    
    @IBAction func didEdit(_ sender: UITextField)
    {
        messageField.isUserInteractionEnabled = false
        sender.resignFirstResponder()
        delegate?.userMessageEdited(index: index, newMessage:messageField.text)
    }
}
