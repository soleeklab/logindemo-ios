//
//  ChatTypingCell.swift
//  Yamam
//
//  Created by sameh on 5/20/18.
//  Copyright © 2018 soleek. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView

class ChatTypingCell:UITableViewCell
{
    var loader:NVActivityIndicatorView!
    
    override func awakeFromNib()
    {
        loader = NVActivityIndicatorView(frame: CGRect(x: 10, y: 0, width: (frame.height * 2), height: frame.height), type: .ballPulseSync, color: .gray, padding: nil)
        addSubview(loader)
        loader.startAnimating()
    }

}
