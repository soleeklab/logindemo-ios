//
//  BotChatCell.swift
//  Yamam
//
//  Created by sameh on 5/17/18.
//  Copyright © 2018 soleek. All rights reserved.
//

import Foundation
import UIKit

class BotChatCell:ChatCell
{
    override var message: String!
    {
        didSet{
            messageField.text = message
        }
    }
    @IBOutlet weak var messageField:UILabel!
    
    
}
