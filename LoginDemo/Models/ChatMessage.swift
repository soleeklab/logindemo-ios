//
//  ChatMessage.swift
//  Yamam
//
//  Created by sameh on 5/19/18.
//  Copyright © 2018 soleek. All rights reserved.
//

import Foundation

struct ChatMessage:Equatable
{
    enum messageSender
    {
        case human,bot,validator
    }
    
    var content:String?
    var isNew:Bool
    var sender:messageSender
    var editable:Bool = true
    var id:Int? // id is useless only for identifaying different messages for equatable
    
    
    static func loadingBubble( _ id:Int)->ChatMessage
    {
        return ChatMessage(content: nil, isNew: true, sender: .bot, editable: false, id: id)
    }
}
extension ChatMessage
{
    init(content: String?, isNew: Bool, sender: messageSender,id:Int = 0)
    {
        self.content = content
        self.isNew = isNew
        self.sender = sender
        self.id = id 
    }
    
}
