//
//  User.swift
//  Yamam
//
//  Created by sameh on 5/20/18.
//  Copyright © 2018 soleek. All rights reserved.
//

import Foundation

struct User:Codable,Jsonable
{
    var id:Int
    var firstName:String
    var lastName:String
    var email:String?
    var phone:String
    var countryCode:String
    var token:String
    
    enum CodingKeys: String, CodingKey
    {
        case id,email,token
        case firstName = "first_name"
        case lastName = "last_name"
        case phone = "phone_number"
        case countryCode = "country_code"
    }
    
    static private var mutableShared:User? =
    {
        return createUserShared()
    }()
    
    static var shared:User?
    {
        get
        {
            return mutableShared
        }
    }
    
    static func distroySharedUser()
    {
        mutableShared = nil
        mutableShared = createUserShared()
    }
    
    static private func createUserShared()->User?
    {
        guard let data = UserDefaults.standard.value(forKey:"UserData") as? Data else
        {
            return nil
        }
        
        let user = try? PropertyListDecoder().decode(User.self, from: data)
        return user
    }
}

extension User
{
    init()
    {
        id = 0
        firstName = ""
        lastName = ""
        phone = ""
        countryCode = ""
        token = ""
    }
}
