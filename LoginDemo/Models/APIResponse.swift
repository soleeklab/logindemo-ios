//
//  APIResponse.swift
//  Yamam
//
//  Created by sameh on 5/20/18.
//  Copyright © 2018 soleek. All rights reserved.
//
import Foundation

struct ApiResponse:Jsonable
{
    var code:Int
    var success:Bool = false
    var message:String
    var error:ApiError?
    var data:Any?
}

extension ApiResponse
{
    init(success:Bool,message:String)
    {
        self.success = success
        self.message = message
        code = 0
    }
    
    init( _ dict:NSDictionary)
    {
        code = dict["code"] as? Int ?? 0
        success = (code == 200 )
        message = dict["message"] as? String ?? ""
        data = dict["data"] 
        error = (dict["errors"] as? NSDictionary)?.getObject()
    }
}

struct ApiError:Codable,Jsonable
{
    var message:String
//    var details:String
    
    enum CodingKeys: String, CodingKey
    {
        case message = "errorMessage"
//        case details = "errorDetails"
    }
}
