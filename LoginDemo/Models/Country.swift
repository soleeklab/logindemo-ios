//
//  Country.swift
//  Yamam
//
//  Created by sameh on 5/20/18.
//  Copyright © 2018 soleek. All rights reserved.
//

import Foundation

struct Country{
    var name:String
    var code:String
}
