//
//  ChatVC.swift
//  Yamam
//
//  Created by sameh on 5/17/18.
//  Copyright © 2018 soleek. All rights reserved.
//

import Foundation
import UIKit

class ChatVC:UIViewController
{
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var authunticationBar:UIStackView!
    @IBOutlet weak var verifyPhoneView:UIView!
    @IBOutlet weak var messageField:UITextField!
    @IBOutlet weak var messageFieldBConstraint: NSLayoutConstraint!
    @IBOutlet weak var footerViewHConstraint: NSLayoutConstraint!
    @IBOutlet weak var progressView:ProgressBarUI!
    @IBOutlet weak var countryCode:UIButton!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var reSend: ExtendedButton!
    @IBOutlet weak var errorMessage:UILabel!
    @IBOutlet weak var changePhone:UIButton!
    var transition:KWTransition!
    var botQuestions = [ChatMessage]()
    
    var chatViewModel:AuthenticationViewModel!
    {
        didSet
        {
            chatViewModel.delegate = self
        }
    }
    
    private var disableChat:Bool!
    {
        didSet
        {
            sendButton.isDisabled = disableChat
            messageField.isUserInteractionEnabled = true
        }
    }
    
    var conversation = [ChatMessage(content: "Hi, I’m Yamam. What do you want to do?", isNew: false, sender: .bot)]
    
    override func viewDidLoad()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        configureViewControllerAnimation()        
    }

    //MARK: - Keyboard
    deinit
    {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardWillShow(_ notification: Notification)
    {
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue
        {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            messageFieldBConstraint.constant += keyboardHeight
            view.setNeedsLayout()
            view.layoutIfNeeded()
            tableView.scrollToRow(at: IndexPath(row: conversation.count - 1, section: 0), at: .bottom, animated: true)
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        messageFieldBConstraint.constant = 15
        view.setNeedsLayout()
        view.layoutIfNeeded()
        tableView.scrollToRow(at: IndexPath(row: conversation.count - 1, section: 0), at: .bottom, animated: true)
    }
}



extension ChatVC
{
    //MARK: - Actions
    @IBAction func onSignIn( _ sender:UIButton)
    {
        chatViewModel = LoginVM()
        onAuthunticationAction("Sign in")
    }
    
    @IBAction func onSignUp( _ sender:UIButton)
    {
        chatViewModel = RegisterVM()
        onAuthunticationAction("Sign up")
    }
    
    @IBAction func onResend( _ sender:UIButton)
    {
        let userDict = (User.shared?.convertToDictionary()) as! [String:Any]
        reSend.isDisabled = true
        changePhone.isDisabled = true
        
        APIService(parameters: userDict, URL: .resendCode)
        {
            response in
            if response.success
            {
                self.progressView.timeInterval = 0.1
            }
            
        }
    }
    
    @IBAction func onChangePhone( _ sender:UIButton)
    {
        let cell = tableView.cellForRow(at: IndexPath(row: conversation.count - 2, section: 0)) as! HumanChatCell
        cell.onEdit(sender)
    }
    
    @IBAction func onSend( _ sender:UIButton)
    {
        if let message = messageField.text , message.count > 0
        {
            authunticationBar.isHidden = true
            errorMessage.isHidden = true
            verifyPhoneView.isHidden = true
            
            footerViewHConstraint.constant = 0
            conversation = conversation.filter { $0.sender != .validator }
            
            if let error = chatViewModel.validate(question:botQuestions.first , answer: message)
            {
                errorMessage.text = error
                errorMessage.isHidden = false
                return
            }
            
            var message = ChatMessage(content: message, isNew: true, sender: .human)
            
            if !countryCode.isHidden
            {
                message.editable = false
            }

            countryCode.isHidden = true
            conversation.append(message)
            addMessage()
            messageField.text = nil
            
            if botQuestions.first?.content != nil
            {
                botReply()
            }
            else
            {
                self.conversation.append(botQuestions[0])
                botQuestions.removeFirst()
                self.tableView.reloadData()
                self.tableView.scrollToRow(at: IndexPath(row: self.conversation.count - 1 , section: 0), at: .bottom, animated: true)
            }
            
        }
    }
    
    @IBAction func onPhonePicking( _ sender:UIButton)
    {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "CountryPickerVC")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func onAuthunticationAction( _ string:String)
    {
        disableChat = false
        conversation.append(ChatMessage(content: string, isNew: true, sender: .human,editable:true,id:nil))
        footerViewHConstraint.constant = 0
        authunticationBar.isHidden = true
        addMessage()
        botReply()
    }
    
    func onVerifyPhone()
    {
        footerViewHConstraint.constant = 80
        verifyPhoneView.isHidden = false
        progressView.timeInterval = 0.1
        progressView.delegate = self
        reSend.isDisabled = true
        changePhone.isDisabled = true
        
        
//        reSend.setNeedsLayout()
//        reSend.layoutIfNeeded()
        
//        reSend.gradient.removeFromSuperlayer()
//        reSend.gradientBorder = true
        
        reSend.borderColor = .black
    }
    
    func botReply(withWaiting:Bool = true)
    {
        disableChat = true
        if withWaiting
        {
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100))
            {
                self.conversation.append(ChatMessage(content: nil, isNew: true, sender: .bot))
                self.tableView.reloadData()
                self.tableView.scrollToRow(at: IndexPath(row: self.conversation.count - 1 , section: 0), at: .bottom, animated: true)
            }
        }
        let time:DispatchTime = .now() +  ( withWaiting ? .milliseconds(900) : .milliseconds(1))
        
        DispatchQueue.main.asyncAfter(deadline: time)
        {
            self.conversation.removeLast()
            self.conversation.append(self.botQuestions.first!)
            self.addMessage()
            self.botQuestions.removeFirst()
            self.disableChat = false
        }
    }
    
}


extension ChatVC:UITableViewDelegate,UITableViewDataSource
{
    //MARK: - Message Table
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return conversation.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let message = conversation[indexPath.row]
        
        if message.content == nil , message.sender == .bot
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "waitingCell") as! ChatTypingCell
            return cell
        }
        
        let cell = ([.bot,.validator].contains(message.sender)) ?
            tableView.dequeueReusableCell(withIdentifier: "botCell", for: indexPath) as! BotChatCell :
            tableView.dequeueReusableCell(withIdentifier: "humanCell", for: indexPath) as! HumanChatCell
        
        (cell as? HumanChatCell)?.delegate = self
        (cell as? HumanChatCell)?.index = indexPath.row
        (cell as? HumanChatCell)?.editButton.isHidden = !message.editable
        
        cell.message = message.content
       
        
        
        if message.isNew
        {
            cell.topConstraint.constant = 300
            conversation[indexPath.row].isNew = false
        }
        else
        {
            cell.topConstraint.constant = 0
            cell.layoutSubviews()
            cell.layoutIfNeeded()
            
        }
        
        return cell
    }
}

extension ChatVC:ConversationDelegate
{
    //MARK: - API Functions
    func loginUser(user: User)
    {
            disableChat = true
            disableHumanEdits()
            footerViewHConstraint.constant = 0
            verifyPhoneView.isHidden = true
            
            Authentication.login(user: user)
            {
                response in
                if response.success
                {
                    self.botReply(withWaiting: false)
                    self.onVerifyPhone()
                }
                else
                {
                    self.conversation.removeLast()
                    self.conversation.append(ChatMessage(content: response.message, isNew: true, sender: .bot))
                    self.addMessage()
                    self.disableChat = false
                    
                    //not registered user
                    self.botQuestions = self.chatViewModel.botQuestions
                    self.botQuestions.removeFirst()
                    self.phoneNumberUI()
                }
            }
        }
    
    func verifyUser(user: User, code: String)
    {
        disableChat = true
        
        Authentication.verifyUser(code: code, user: user)
        {
            response in
            
            if response.success
            {
                self.segueToWelcome()
            }
            else
            {
                self.conversation.removeLast()
                self.conversation.append(ChatMessage(content: response.message, isNew: true, sender: .bot))
                self.addMessage()
                self.disableChat = false
                self.botQuestions.append(self.chatViewModel.botQuestions.last!)
                
            }
        }
    }
    
    func registerUser(user: User)
    {
        disableChat = true
        disableHumanEdits()
        Authentication.register(user: user)
        {
            response in
            
            if response.success
            {
                self.botReply(withWaiting: false)
                self.disableEdits()
                self.onVerifyPhone()
            }
            else
            {
                self.conversation.removeLast()
                self.conversation.append(ChatMessage(content: response.message, isNew: true, sender: .bot))
                self.addMessage()
                self.disableChat = false
                
                //number registered
                self.botQuestions = self.chatViewModel.botQuestions
                self.botQuestions.removeFirst(3)
                self.phoneNumberUI()
            }
        }
    }
    
    func phoneNumberUI()
    {
        countryCode.isHidden = false
    }
    
    func selectedCountryCode(country: Country)
    {
        countryCode.setTitle(String(country.code), for: .normal)
    }
    
}

extension ChatVC
{
    //Helper function
    func disableHumanEdits()
    {
        self.conversation = self.conversation.map
            {
                if $0.sender == .human
                {
                    var _message = $0
                    _message.editable = false
                    return _message
                }
                return $0
        }
        var firstMsg = self.conversation.filter { $0.sender == .human }[0]
        firstMsg.editable = true
        self.conversation[1] = firstMsg
        self.tableView.reloadData()
    }
    
    func addMessage()
    {
        let index = IndexPath(row: conversation.count - 1  , section: 0)
        tableView.reloadData()
        tableView.layoutIfNeeded()
        
        if let cell = tableView.cellForRow(at: index) as? ChatCell
        {
            animateCell(at:cell )
        }
        else
        {
            tableView.scrollToRow(at: index, at: .bottom, animated: true)
        }
    }
    
    func animateCell( at cell:ChatCell)
    {
        UIView.animate(withDuration: 0.2,delay:0,options:.curveEaseOut, animations:
        {
            cell.topConstraint.constant = 0
            cell.layoutSubviews()
            cell.layoutIfNeeded()
        }, completion:
        {
            finished in
            
            self.tableView.reloadData()
            if(self.conversation.count >= 1 )
            {
                let index = IndexPath(item: self.conversation.count - 1 , section: 0)
                self.tableView.scrollToRow(at:index , at: .bottom, animated: false)
            }
        })
        
    }
    
    func autoSubmit()
    {
        if conversation[1].content == "Sign up"
        {
            if chatViewModel.user.firstName != "" ,
                chatViewModel.user.lastName != "" ,
                chatViewModel.user.phone != ""
            {
                registerUser(user: chatViewModel.user)
            }
        }
        else if conversation[1].content == "Sign in"
        {
            if chatViewModel.user.phone != "",
                let question = (chatViewModel as? LoginVM)?.botQuestions.last
            {
                botQuestions.append(question)
                loginUser(user: chatViewModel.user)
            }
        }
    }
    
    func disableEdits()
    {
        self.botQuestions.enumerated().forEach
            {
                index,question in
                self.botQuestions[index].editable = false
        }
    }

    func segueToWelcome()
    {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()!
        self.present(vc, animated: true, completion: nil)
    }
}

extension ChatVC:ProgressBarUIDelegate
{
    func progressDidFinish()
    {
        reSend.isDisabled = false
        changePhone.isDisabled = false
    }
}

extension ChatVC:FlipOverTransitionP
{
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning?
    {
        return self.dismissed()
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning?
    {
        return self.presented()
    }
}

extension ChatVC:ChatMessageDelegate
{
   
    func userStartedEdit(index:Int)
    {
        if index == 1 // sign in or sign up
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChatNav")
            (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController = vc
        }
    }
    
    func userMessageEdited(index: Int, newMessage: String?)
    {
        guard
            let newMessage = newMessage,
            newMessage.count > 0
            else
        {
            self.alert(message: "This field can't be empty")
            return
        }
        
        conversation[index].content = newMessage
        if conversation[1].content == "Sign up"
        {
            switch index
            {
            case 3:
                chatViewModel.user.firstName = newMessage
                break
                
            case 5:
                chatViewModel.user.lastName = newMessage
                break
            case 7 :
                chatViewModel.user.phone = newMessage
            default:
                break
            }
        }
        else if conversation[1].content == "Sign in"
        {
            if index == 3
            {
                chatViewModel.user.phone = newMessage
            }
        }
        autoSubmit()
    }
}
