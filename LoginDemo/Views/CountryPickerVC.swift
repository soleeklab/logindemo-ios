//
//  CountryPickerVC.swift
//  Yamam
//
//  Created by sameh on 5/20/18.
//  Copyright © 2018 soleek. All rights reserved.
//

import UIKit

class CountryPickerVC: UITableViewController
{
    var countries = [Country(name: "Egypt", code: "+20"),Country(name: "Saudi", code: "+900")]
    
    override func viewDidLoad()
    {
        tableView.tableFooterView = UIView()
        tableView.sizeToFit()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return countries.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let vc = navigationController?.previousVC() as! ChatVC
        vc.chatViewModel.selectedCountryCode = countries[indexPath.row]
        navigationController?.popViewController(animated: true)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = "\(countries[indexPath.row].code) \(countries[indexPath.row].name)"
        return cell
    }
}
