//
//  RegisterVC.swift
//  Yamam
//
//  Created by sameh on 5/20/18.
//  Copyright © 2018 soleek. All rights reserved.
//

import Foundation
import UIKit

class RegisterVM:AuthenticationViewModel
{
    var user: User! = User()
    weak var delegate:ConversationDelegate?
    {
        didSet
        {
            delegate?.botQuestions = botQuestions
        }
    }
    
    var selectedCountryCode:Country = Country(name: "Egypt", code: "+20")
    {
        didSet
        {
            delegate?.selectedCountryCode(country: selectedCountryCode)
        }
    }

    
    var botQuestions =
    [
        ChatMessage(content: "Hi, What's your first name?", isNew: true, sender: .bot),
        ChatMessage(content: "What's your last name?", isNew: true, sender: .bot),
        ChatMessage(content: "Ok, Send me your Phone number🤗", isNew: true, sender: .bot),
        ChatMessage.loadingBubble(1),
        ChatMessage(content: "You’ll Recieve a pin in 30 seconds.", isNew: true, sender: .bot),
        ChatMessage.loadingBubble(2)
    ]
    
    
    func validate(question:ChatMessage?,answer:String)->String?
    {
        guard
            let question = question ,
            let index = botQuestions.index(of: question)
            else { return nil }
        
        switch index {
        case 1:
            user.firstName = answer
            break
            
        case 2:
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500))
            {
                self.delegate?.phoneNumberUI()
            }
            user.lastName = answer
            break
    
        case 3:
            let isValid = DataValidator.validatePhone(value: answer)
            if isValid
            {
                user.phone = answer
                user.countryCode = selectedCountryCode.code
                delegate?.registerUser(user: user)
                
            }
            return isValid ? nil : "Please enter a valid phone"
        
        case 4...10:
            delegate?.verifyUser(user: user, code: answer)
            break
            
        default:
            break
        }
        return nil 
    }
    
}
