//
//  RegisterVC.swift
//  Yamam
//
//  Created by sameh on 5/20/18.
//  Copyright © 2018 soleek. All rights reserved.
//

import Foundation
import UIKit

class LoginVM:AuthenticationViewModel
{
    var user: User! = User()
    weak var delegate:ConversationDelegate?
    {
        didSet
        {
            delegate?.botQuestions = botQuestions
            delegate?.phoneNumberUI()
        }
    }
    var selectedCountryCode:Country = Country(name: "Egypt", code: "+20")
    {
        didSet
        {
            delegate?.selectedCountryCode(country: selectedCountryCode)
        }
    }
    
    var botQuestions = [
        ChatMessage(content: "Please Send me your Phone number🤗", isNew: true, sender: .bot),
        ChatMessage.loadingBubble(1),
        ChatMessage(content: "You’ll Recieve a pin in 30 seconds.", isNew: true, sender: .bot),
        ChatMessage.loadingBubble(2)
    ]
    
    init()
    {
        delegate?.botQuestions = botQuestions
    }
    
    func validate(question:ChatMessage?,answer:String)->String?
    {
        guard let question = question ,
            let index = botQuestions.index(of: question) else { return nil }
        
        switch index
        {
        case 1:
            let isValid = DataValidator.validatePhone(value: answer)
            if isValid
            {
                user.phone = answer
                user.countryCode = selectedCountryCode.code
                delegate?.loginUser(user: user)
            }
            return isValid ? nil : "Please enter a valid phone"
            
        case 3:
        
            delegate?.verifyUser(user: user, code: answer)
            break
            
        default:
            break
        }
        return nil
    }
    
}
